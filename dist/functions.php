<?php
add_theme_support( 'post-thumbnails' );

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();	
}

function register_my_menu() {
	register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page( array(
		'page_title'	=>	'Főoldal mezők',
		'menu_title'	=>	'Főoldal mezők',
		'menu-slug'		=>	'fooldal-mezok',
		'capability'	=> 	'edit_posts',
		'parent_slug'	=>	'',
		'position'		=> 	1,
		'icon_url'		=> 	false,
	));	
}

function custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/* Register Custom Navigation Walker */
// function register_navwalker(){
// 	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
// }
// add_action( 'after_setup_theme', 'register_navwalker' );