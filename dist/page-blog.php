<?php
/*

Template Name: Blog

*/

get_header();

?>
<section id="blogHero">
    <div class="container">
        <div class="grid grid-2">
            <div class="item-left">
                <img src="<?php the_field('main_image'); ?>" alt="Karolina Lunk">
            </div>
            <div class="item-right">
                <p><?php the_field('small_title'); ?></p>
                <p class="lead"><?php the_field('lead_text'); ?></p>
                <a href="" class="mobile-only">Olvass Tovább</a>
            </div>
        </div>
    </div>
</section>
<section id="blogPosts" class="bg--grey-light">
    <div class="container">
        <h2>Bejegyzések</h2>
        <div class="grid grid-3">

            <?php $loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 3 ) ); ?>

            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            
                <div class="grid-item">
                    <img src="<?php echo the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                    <p class="date"><?php echo get_the_date(); ?></p>
                    <h3><?php the_title(); ?></h3>
                    <p class="excerpt"><?php echo get_the_excerpt(); ?></p>
                    <a href="<?php echo get_permalink(); ?>">Olvass Tovább</a>
                </div>

            <?php endwhile; wp_reset_query(); ?>

        </div>
    </div>
</section>
<section id="subscribe">
    <div class="container">
        <div class="grid grid-2">
            <div class="grid-item item-left">
                <h3 class="lead">Hasznos infóból sosem elég!</h3>
                <div class="line--short"></div>
                <p>Elérhetőségedért cserébe rendszeres, hasznos olvasnivalóval látlak el edzésről, étkezésről, receptekről, életmódról.</p>
            </div>
            <div class="grid-item item-right">
                <form action="https://gmail.us10.list-manage.com/subscribe/post?u=e8aba516a793ffd9033728886&amp;id=a99ecaa1f8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <div class="mc-field-group">
                            <input type="text" value="" name="FNAME" class="" placeholder="Név" id="mce-FNAME">
                        </div>
                        <div class="mc-field-group">
                            <input type="email" value="" name="EMAIL" placeholder="E-mail cím" class="required email" id="mce-EMAIL">
                        </div>
                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e8aba516a793ffd9033728886_a99ecaa1f8" tabindex="-1" value=""></div>
                        <div class="clear"><input type="submit" value="Feliratkozás" name="subscribe" id="mc-embedded-subscribe" class="button btn"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>