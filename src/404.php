<?php get_header(); ?>

	<section id="oops">
		<div class="container">
			<h2><?php _e( 'Oops! That page can&rsquo;t be found.', 'karolinalunk' ); ?></h2>
			<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'karolinalunk' ); ?></p>
		</div>
	</section><!-- #primary -->

<?php get_footer();
