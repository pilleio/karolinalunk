</main>
<footer>
    <div class="container">
        <div class="grid grid-4">
            <div class="grid-item">
                <img src="<?php bloginfo('template_url') ?>/assets/img/logo.svg" alt="Karolina Lunk">
            </div>
            <div class="grid-item">
                <p><a href="<?php echo get_home_url(); ?>/coaching">Coaching</a></p>
                <p><a href="<?php echo get_home_url(); ?>/blog">Blog</a></p>
                <p><a href="<?php echo get_home_url(); ?>/kapcsolat">Kapcsolat</a></p>
            </div>
            <div class="grid-item">
                <p><a href="mailto:<?php the_field('email_cim', 'option'); ?>"><?php the_field('email_cim', 'option'); ?></a></p>
                <p><a href="tel:<?php the_field('telefonszam', 'option'); ?>"><?php the_field('telefonszam', 'option'); ?></a></p>
            </div>
            <div class="grid-item text-right">
                <span><a href="<?php the_field('facebook_link', 'option'); ?>"><img src="<?php bloginfo('template_url') ?>/assets/img/facebook.svg" alt="Karolina Lunk facebook"></a></span>
                <span><a href="<?php the_field('instagram_link', 'option'); ?>"><img src="<?php bloginfo('template_url') ?>/assets/img/instagram.svg" alt="Karolina Lunk instagram"></a></span>
            </div>
        </div>
    </div>
</footer>

<script src="<?php bloginfo('template_url') ?>/assets/js/scripts.js"></script>
<?php wp_footer(); ?>
</body>
</html>