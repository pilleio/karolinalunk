<?php
/*

Template Name: Main Page

*/

get_header();

?>

<section id="hero" class="bg--grey-light">
	<div class="grid grid--hero-grid grid--padding-left">
		<h2 class="lead"><?php the_field('hero_szlogen', 'option'); ?></h2>
		<p><?php the_field('hero_text', 'option'); ?></p>
		<a href="#services" class="btn">Vágjunk bele!</a>
		<img src="<?php the_field('hero_kep', 'option'); ?>" alt="">
	</div>
	<div class="scroll-wrapper">
		<img src="<?php bloginfo('template_url') ?>/assets/img/arrow_down.svg" alt="arrow_down">
		<small>görgess le</small>
	</div>
</section>
<section id="services" class="bg--grey-light">
	<div class="container">
		<div class="grid grid--services-grid">
			<h2>Szolgáltatásaim</h2>
			<div class="lead">
				<div class="line--short"></div>
				<h3><?php the_field('szolgaltatasaim_lead', 'option'); ?></h3>
			</div>
			<div class="text">
			<?php the_field('szolgaltatasaim_szoveg', 'option'); ?>
			</div>
			<?php the_field('szolgaltatasaim_lista', 'option'); ?>
			<img src="<?php the_field('szolgaltatasaim_kep', 'option'); ?>" alt="Karolina Lunk - Szolgáltatásaim">
		</div>
	</div>
</section>
<section id="services-2" class="bg--grey-light bg-mobile--white">
	<div class="container">
		<h3 class="lead"><?php the_field('szolgaltatasaim_lead_2', 'option'); ?></h3>
		<div class="grid grid-3">
			<div class="grid-item item-left">
				<h3><span>#01</span><br><?php the_field('szolgaltatasaim_lepesek_cim_1', 'option'); ?></h3>
				<div class="line--short"></div>
				<p><?php the_field('szolgaltatasaim_lepesek_szoveg_1', 'option'); ?></p>
			</div>
			<div class="grid-item item-center">
				<h3><span>#02</span><br><?php the_field('szolgaltatasaim_lepesek_cim_2', 'option'); ?></h3>
				<div class="line--short"></div>
				<p><?php the_field('szolgaltatasaim_lepesek_szoveg_2', 'option'); ?></p>
			</div>
			<div class="grid-item item-right">
				<h3><span>#03</span><br><?php the_field('szolgaltatasaim_lepesek_cim_3', 'option'); ?></h3>
				<div class="line--short"></div>
				<p><?php the_field('szolgaltatasaim_lepesek_szoveg_3', 'option'); ?></p>
			</div>
		</div>
		<a href="<?php echo get_home_url(); ?>/coaching#packages" class="btn">Válogass a fitnesz csomagok közül!</a>
	</div>
</section>
<section id="about" class="bg--white">
	<div class="container">
		<div class="grid grid-2">
			<div class="grid-item item-left">
				<h2>Rólam</h2>
				<div class="line--short"></div>
				<p><?php the_field('rolam_szoveg', 'option'); ?></p>
				<a href="<?php echo get_home_url(); ?>/kapcsolat" class="btn">Contact me</a>
			</div>
			<div class="grid-item item-right">
				<img src="<?php the_field('rolam_kep', 'option'); ?>" alt="Karolina Lunk - Rólam">
			</div>
		</div>
	</div>
</section>
<section id="subscribe" class="bg--grey-light">
	<div class="container">
		<div class="grid grid-2">
			<div class="grid-item item-left">
				<h3 class="lead">Hasznos infóból sosem elég!</h3>
				<div class="line--short"></div>
				<p>Elérhetőségedért cserébe rendszeres, hasznos olvasnivalóval látlak el edzésről, étkezésről, receptekről, életmódról.</p>
			</div>
			<div class="grid-item item-right">
				<form action="https://gmail.us10.list-manage.com/subscribe/post?u=e8aba516a793ffd9033728886&amp;id=a99ecaa1f8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div id="mc_embed_signup_scroll">
						<div class="mc-field-group">
							<input type="text" value="" name="FNAME" class="" placeholder="Név" id="mce-FNAME">
						</div>
						<div class="mc-field-group">
							<input type="email" value="" name="EMAIL" placeholder="E-mail cím" class="required email" id="mce-EMAIL">
						</div>
						<div id="mce-responses" class="clear">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none"></div>
						</div>
						<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_e8aba516a793ffd9033728886_a99ecaa1f8" tabindex="-1" value=""></div>
						<div class="clear"><input type="submit" value="Feliratkozás" name="subscribe" id="mc-embedded-subscribe" class="button btn"></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<!-- <section id="instagram">
	<div class="container">
		<h2>Kövess <br>Instagramon!</h2>
		<p class="text-right"><small>@kthept</small></p>
		<div id="instafeed"></div>
	</div>
	<script src="<?php bloginfo('template_url') ?>/assets/js/scripts.js"></script>
	<script type="text/javascript">
		var userFeed = new Instafeed({
			get: 'user',
			userId: '1425129586',
			limit: 6,
			template: '<div class="instagram-picture" style="border: none !important; --aspect-ratio: 1/1; position: relative;"><a href="{{link}}" target="blank"><div class="instaPic" style="background-image: url({{image}}); background-size: cover; background-position: center center; width: 100%; height: 100%;"></div></a></div>',
			accessToken: '1425129586.2a46ab3.dad122e9fbcb4582b6e19afa7400371e',
			resolution: 'standard_resolution'
		});
		userFeed.run();
	</script>
</section> -->

<?php

get_footer();

?>
