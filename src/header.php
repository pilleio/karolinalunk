<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Karolina Lunk - Fitness Coach</title>

	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/style.css">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
	<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header class="bg--grey-light">
    <div id="navbar">
        <div id="header-logo-wrapper">
            <a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('template_url') ?>/assets/img/logo.svg" alt="Karolina Lunk logo"></a>
        </div>
        <nav>
            <ul>
                <li><a href="<?php echo get_home_url(); ?>/coaching">Coaching</a></li>
                <li><a href="<?php echo get_home_url(); ?>/blog">Blog</a></li>
                <li><a href="<?php echo get_home_url(); ?>/kapcsolat">Kapcsolat</a></li>
                <li>
                    <span><a href="<?php the_field('facebook_link', 'option'); ?>"><img src="<?php bloginfo('template_url') ?>/assets/img/facebook.svg" alt="Karolina Lunk facebook"></a></span>
                    <span><a href="<?php the_field('instagram_link', 'option'); ?>"><img src="<?php bloginfo('template_url') ?>/assets/img/instagram.svg" alt="Karolina Lunk instagram"></a></span>
                </li>
            </ul>
        </nav>
    </div>
    <div id="mobileMenu">
        <div id="mobile-header-logo-wrapper">
            <a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('template_url') ?>/assets/img/logo.svg" alt="Karolina Lunk logo"></a>
        </div>
        <button class="hamburger hamburger--squeeze" type="button">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
          </button>
        <nav id="mobileNavbar">
            <ul>
                <li><a href="<?php echo get_home_url(); ?>">Home</a></li>
                <li><a href="<?php echo get_home_url(); ?>/coaching">Coaching</a></li>
                <li><a href="<?php echo get_home_url(); ?>/blog">Blog</a></li>
                <li><a href="<?php echo get_home_url(); ?>/kapcsolat">Kapcsolat</a></li>
            </ul>
        </nav>
    </div>
</header>
<main>