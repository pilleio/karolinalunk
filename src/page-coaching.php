<?php
/*

Template Name: Coaching

*/

get_header();

?>

<section id="coaching" class="bg--white">
    <div class="container">
        <h1 class="site-title">Coaching</h1>
    </div>
    <div class="grid grid-2 grid--coaching-grid">
        <?php if(have_rows('fejlec_tartalom')): ?>
            <?php while(have_rows('fejlec_tartalom')): the_row(); ?>
                <div class="grid-item item-left" style="background-image: url('<?php the_sub_field('fejlec_kep');?>');"></div>
                <div class="grid-item item-right">
                    <?php the_sub_field('fejlec_szoveg');?>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
        
        <div class="container mobile-only">
            <?php if(have_rows('fejlec_tartalom')): ?>
                <?php while(have_rows('fejlec_tartalom')): the_row(); ?>
                    <img src="<?php the_sub_field('fejlec_kep');?>" alt="Coaching">
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<section id="modernCoaching" class="bg--grey-light">
    <div class="container">
        <h3 class="lead">Válaszd a coaching legmodernebb formáját!</h3>
        <div class="grid grid-2">
            <div class="grid-item item-left">
                <p>A csodadiéták és hangzatos edzésformák útvesztőjében nehéz eldönteni, mi is igazán szükséges számunkra egy egészséges, örömteli életmód fenntartásához.</p>
                <p>Az igazság azonban az, hogy ehhez nem kell súlyos pénzeket költenünk sem divatos felszerelésekre, sem drága kiegészítőkre, sem akár konditermi tagságra. Ráadásul hatalmas áldozatokat sem kell hoznod, csupán követned pár egyszerű alapelvet.</p>
            </div>
            <div class="grid-item grid--coaching-list-grid">
                <div>
                    <p class="list-item--num">1</p>
                    <h4 class="list-item--title">Te vagy kontrollban</h4>
                    <p class="list-item--description">
                        Szakmai támogatás mellett, ám Te magad vagy a változásod fő irányítója. A kezdetektől önbizalmat építhesz, amint lépésenként megtapasztalod, milyen könnyű is követni az új életmódod.
                    </p>
                </div>
                <div>
                    <p class="list-item--num">2</p>
                    <h4 class="list-item--title">Teljeskörű szolgáltatás a nap 24 órájában</h4>
                    <p class="list-item--description">
                        Sokkal több és sokrétűbb információt és támogatást kapsz, mintha csak heti kétszer vagy háromszor 1 órában találkoznál edződdel. 
                    </p>
                </div>
                <div>
                    <p class="list-item--num">3</p>
                    <h4 class="list-item--title">Bárhol és bármikor végezhető</h4>
                    <p class="list-item--description">
                        Edzőteremben vagy otthon, eszközökkel vagy saját testsúllyal, reggel, délben vagy este. A programod mindig rendelkezésedre áll, bárhol is jársz! 
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="programs" class="bg--white">
    <div class="container">
        <div class="grid grid-2">
            <h2>Mit is tartalmaznak a programok?</h2>
        </div>
        <div class="grid grid-3">

            <?php if(have_rows('programok_tartalma')): ?>
                <?php while(have_rows('programok_tartalma')): the_row(); ?>
                    <div class="grid-item">
                        <img src="<?php the_sub_field('blokk_1_kep'); ?>" alt="<?php the_sub_field('blokk_1_cim'); ?>">
                        <h3><?php the_sub_field('blokk_1_cim'); ?></h3>
                        <div class="line--short"></div>
                        <p><?php the_sub_field('blokk_1_szoveg'); ?></p>
                    </div>
                    <div class="grid-item">
                        <img src="<?php the_sub_field('blokk_2_kep'); ?>" alt="<?php the_sub_field('blokk_2_cim'); ?>">
                        <h3><?php the_sub_field('blokk_2_cim'); ?></h3>
                        <div class="line--short"></div>
                        <p><?php the_sub_field('blokk_2_szoveg'); ?></p>
                    </div>
                    <div class="grid-item">
                        <img src="<?php the_sub_field('blokk_3_kep'); ?>" alt="<?php the_sub_field('blokk_3_cim'); ?>">
                        <h3><?php the_sub_field('blokk_3_cim'); ?></h3>
                        <div class="line--short"></div>
                        <p><?php the_sub_field('blokk_3_szoveg'); ?></p>
                    </div>                
                <?php endwhile; ?>
            <?php endif; ?>

        </div>
    </div>
</section>
<img src="<?php the_field('parallax_kep') ?>" alt="Coaching" class="parallax">
<section id="packages" class="bg--white">
    <div class="container">
        <h2>Fitnesz Coaching Csomagok</h2>
        <div class="grid grid-4 grid-no-gap grid--packages-grid">
            <div class="grid-item grid-item--titles">
                <!-- list of hybrid package names -->
                <div></div>
                <?php
                if( have_rows('csomagok_tartalma') ):
                    while( have_rows('csomagok_tartalma') ) : the_row(); ?>
                        <div><p><?php the_sub_field('szolgaltatas_neve') ?></p></div>
                    <?php endwhile;
                endif; ?>
            </div>
            <div class="grid-item grid-item--package-1">
                <div class="package-name">
                    <h3><?php the_field('csomag_1_neve') ?></h3>
                    <p class="price"><?php the_field('csomag_1_ara') ?></p>
                </div>
                <?php
                if( have_rows('csomagok_tartalma') ):
                    while( have_rows('csomagok_tartalma') ) : the_row(); ?>
                        <div>
                            <?php 
                            $service = get_sub_field('csomagok');
                            if($service && in_array('Csomag 1', $service)) { ?>

                                <img src="<?php bloginfo('template_url') ?>/assets/img/package-item-true.svg" alt="">
                            
                            <?php } else {?>

                                <img src="<?php bloginfo('template_url') ?>/assets/img/package-item-false.svg" alt="">
                            
                            <?php } ?>
                        </div>
                    <?php endwhile;
                endif; ?>
            </div>
            <div class="grid-item grid-item--package-2">
                <div class="package-name">
                    <h3><?php the_field('csomag_2_neve') ?></h3>
                    <p class="price"><?php the_field('csomag_2_ara') ?></p>
                </div>
                <?php
                if( have_rows('csomagok_tartalma') ):
                    while( have_rows('csomagok_tartalma') ) : the_row(); ?>
                        <div>
                            <?php 
                            $service = get_sub_field('csomagok');
                            if($service && in_array('Csomag 2', $service)) { ?>

                                <img src="<?php bloginfo('template_url') ?>/assets/img/package-item-true.svg" alt="">
                            
                            <?php } else {?>

                                <img src="<?php bloginfo('template_url') ?>/assets/img/package-item-false.svg" alt="">
                            
                            <?php } ?>
                        </div>
                    <?php endwhile;
                endif; ?>
            </div>
            <div class="grid-item grid-item--package-3">
                <div class="package-name">
                    <h3><?php the_field('csomag_3_neve') ?></h3>
                    <p class="price"><?php the_field('csomag_3_ara') ?></p>
                </div>
                <?php
                if( have_rows('csomagok_tartalma') ):
                    while( have_rows('csomagok_tartalma') ) : the_row(); ?>
                        <div>
                            <?php 
                            $service = get_sub_field('csomagok');
                            if($service && in_array('Csomag 3', $service)) { ?>

                                <img src="<?php bloginfo('template_url') ?>/assets/img/package-item-true.svg" alt="">
                            
                            <?php } else {?>

                                <img src="<?php bloginfo('template_url') ?>/assets/img/package-item-false.svg" alt="">
                            
                            <?php } ?>
                        </div>
                    <?php endwhile;
                endif; ?>
            </div>
        </div>
        <div class="mobile-only packages--mobile-grid">
            <div class="grid-item">
                <h3><?php the_field('csomag_1_neve') ?> <span>(<?php the_field('csomag_1_ara') ?>)</span></h3>
                <ul>
                    <?php if( have_rows('csomagok_tartalma') ):
                        while( have_rows('csomagok_tartalma') ) : the_row(); ?>
                            <?php 
                            $service = get_sub_field('csomagok');
                            if($service && in_array('Csomag 1', $service)) { ?>

                                <li><?php the_sub_field('szolgaltatas_neve') ?></li>
                            
                            <?php } ?>
                        <?php endwhile;
                    endif; ?>
                </ul>
            </div>
            <div class="grid-item">
                <h3><?php the_field('csomag_2_neve') ?> <span>(<?php the_field('csomag_2_ara') ?>)</span></h3>
                <ul>
                    <?php if( have_rows('csomagok_tartalma') ):
                        while( have_rows('csomagok_tartalma') ) : the_row(); ?>
                            <?php 
                            $service = get_sub_field('csomagok');
                            if($service && in_array('Csomag 2', $service)) { ?>

                                <li><?php the_sub_field('szolgaltatas_neve') ?></li>
                            
                            <?php } ?>
                        <?php endwhile;
                    endif; ?>
                </ul>
            </div>
            <div class="grid-item">
                <h3><?php the_field('csomag_3_neve') ?> <span>(<?php the_field('csomag_3_ara') ?>)</span></h3>
                <ul>
                    <?php if( have_rows('csomagok_tartalma') ):
                        while( have_rows('csomagok_tartalma') ) : the_row(); ?>
                            <?php 
                            $service = get_sub_field('csomagok');
                            if($service && in_array('Csomag 3', $service)) { ?>

                                <li><?php the_sub_field('szolgaltatas_neve') ?></li>
                            
                            <?php } ?>
                        <?php endwhile;
                    endif; ?>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="programsDescription" class="bg--grey-light">
    <div class="container">
        <h3>Az Online Fitness Coaching programok  segítségével</h3>
        <div class="grid grid-2-1">
            <div class="item-left">
                <ul>
                    <li>Elsajátítod a gyakorlatok szabályos elvégzését</li>
                    <li>Mind fizikailag, mint mentálisan erősebb leszel</li>
                    <li>Megtanulod, hogyan állíts össze magadnak egy egészségesebb étrendet</li>
                    <li>Megváltoztathatod az életed</li>
                    <li>Erősebb, fittebb, egészségesebb, magabiztosabb leszel!</li>
                </ul>
            </div>
            <div class="item-right">
                <h4>Neked szól, ha:</h4>
                <ul>
                    <li>Fittebb</li>
                    <li>Erősebb</li>
                    <li>Magabiztosabb </li>
                    <li>Egészségesebb szeretnél lenni</li>
                    <br>
                    <li>Fogynál</li>
                    <li>Izmot építenél</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="bg--white mobile-only">
        <div class="container">
            <h4>Neked szól, ha:</h4>
                <ul>
                    <li>Fittebb</li>
                    <li>Erősebb</li>
                    <li>Magabiztosabb </li>
                    <li>Egészségesebb szeretnél lenni</li>
                    <br>
                    <li>Fogynál</li>
                    <li>Izmot építenél</li>
                </ul>
        </div>
    </div>
</section>
<section id="contact" class="bg--white">
    <div class="container">
        <h2>Tedd meg a legelső lépést!</h2>
        <div class="grid grid-2">
            <div class="grid-item item-left">
                <p>Küldd el jelentkezésed az Online Fitnesz Csomag megnevezésével és minden olyan infóval, amit hasznosnak tartasz megosztani trénereddel.</p>
                <br>
                <br>
                <p>Leveledre mihamarabb válaszolok.</p>
            </div>
            <div class="grid-item">
                <?php echo do_shortcode( '[contact-form-7 id="37" title="Kapcsolat"]' ); ?>
            </div>
        </div>
        <div class="mobile-only">
            <p><a href="mailto:<?php the_field('email_cim', 'option'); ?>"><?php the_field('email_cim', 'option'); ?></a></p>
            <p><a href="tel:<?php the_field('telefonszam', 'option'); ?>"><?php the_field('telefonszam', 'option'); ?></a></p>
        </div>
    </div>
</section>

<?php

get_footer();

?>
