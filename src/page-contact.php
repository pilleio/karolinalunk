<?php
/*

Template Name: Kapcsolat

*/

get_header();

?>
<section id="contact">
        <div class="container">
            <h2>Kapcsolat</h2>
            <div class="grid grid-2">
                <div class="item-left">
                    <h3 class="lead">Köszönöm, hogy ellátogattál az oldalra!</h3>
                    <p>Kérdéseidet, kéréseidet az űrlap kitöltésével tudod elküldeni. Ígérem, válaszolok, amit tudok :) </p>
                    <div id="contactInfo">
                        <div><a href="mailto:<?php the_field('email_cim', 'option'); ?>"><?php the_field('email_cim', 'option'); ?></a></div>
                        <div><a href="tel:<?php the_field('telefonszam', 'option'); ?>"><?php the_field('telefonszam', 'option'); ?></a></div>
                    </div>
                </div>
                <div class="item-right">
                    <?php echo do_shortcode( '[contact-form-7 id="37" title="Kapcsolat"]' ); ?>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>