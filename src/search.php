<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();

if(isset($_GET['post_type'])) {
    $type = $_GET['post_type'];
    if($type == 'post') {
        load_template(TEMPLATEPATH . '/post-search.php');
    } elseif($type == 'articles') {
        load_template(TEMPLATEPATH . '/knowledge-search.php');
    }
} else {
	load_template(TEMPLATEPATH . '/all-search.php');
}

get_footer();
